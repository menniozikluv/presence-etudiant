import { createStore } from 'vuex'
import createPersistedState from "vuex-persistedstate";


export default createStore({
  state: {
    user: null
  },
  getters: {
    isLoggedIn: state => !!state.user
  },
  mutations: {
    setUser(state, userData) {
      state.user = userData
      console.log("User set in store:", state.user);
    }
  },
  actions: {
    userLogin({ commit }, user) {
      // Ici, connectez-vous à votre API, puis utilisez la mutation pour définir les données de l'utilisateur
      console.log("userLogin action received data:", user);
      commit('setUser', user)
    },
    userLogout({ commit }) {
      commit('setUser', null)
    }
  },
  plugins: [createPersistedState()],
})
