import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/HomeView.vue'
import Dashboard from '@/views/DashboardView.vue'
import store from '@/store/index.js'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/components/LoginComponent.vue')
}

  
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  console.log("Navigating to:", to.name, "isLoggedIn:", store.getters.isLoggedIn);
  if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.isLoggedIn) {
    next('/')
  } else {
    next()
  }
})

export default router
