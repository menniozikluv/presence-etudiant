import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:3003', // Remplacez par l'URL de votre backend si elle est différente
  timeout: 10000, // définit un délai d'expiration
  headers: {
    'Content-Type': 'application/json',
  },
});

api.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  if (token) {
      config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
